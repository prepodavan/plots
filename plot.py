import sys
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt


l = Path(sys.argv[1]).open().readlines()

for i in range(len(l)):
    l[i] = l[i].split('        ')

l[0][0] = l[0][0][1:]
nums = list()
for i in l:
     nums.append([float(i[0]), float(i[1].replace('\n', ''))])

data = np.array(nums).transpose()
plt.plot(data[1], data[0])
plt.ylabel('Сопротивление')
plt.xlabel('Температура')
plt.savefig(sys.argv[2])


